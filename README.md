# NeuralNetSnake
Dieses Projekt ist eine private Studie von Neuronalen Netzten.
Es ist umgesetzt in Java und OpenJavaFX.

Das Programm kann mit maven gestartet werden: `mvn clean javafx:run`

Bereits trainierte Neuronale Netzte sind im Ordner Snake, Um die Simulation zu starten, klicke auf "Start" und wähle im Drop-Down Menü "LoadSnake". Anschließend muss in den Ordner navigiert werden und mit "Start" kann dann die Simulation gestartet werden.

![UI-Image](image.png)

Disclaimer:
Dies ist ein reines Lehr-Projekt welches ich im dritten Semester des Informatik-Studiums erstellt habe, erwartet also keinen Lupenreinen Code ;)
